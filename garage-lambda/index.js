/* eslint-disable  func-names */
/* eslint-disable  no-console */

const Alexa = require('ask-sdk');
const AWS = require('aws-sdk');
// Replace the text in this endpoint with your AWS IoT Thing endpoint from the Interact section.
const iotData = new AWS.IotData({ endpoint: "a3gjpvigaz4dn4-ats.iot.us-west-2.amazonaws.com" });
const GetNewFactHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'LaunchRequest'
      || (request.type === 'IntentRequest'
        && (request.intent.name === 'GarageDoorOpen' || request.intent.name === 'GarageDoorClose'));
  },
  handle(handlerInput) {
   const factArr = '';
    let door = 0;
    if (handlerInput.requestEnvelope.request.intnet.name === 'GarageDoorOpen') {
      const factArr = dataOpen;
      door = 1;
    }
    else if (handlerInput.requestEnvelope.request.intent.name === 'GarageDoorClose') {
      const factArr = dataClose;
    }
    
    const factIndex = Math.floor(Math.random() * factArr.length);
    const randomFact = factArr[factIndex];
    const speechOutput = ALEXA_GARAGE_RESPONSE_MESSAGE + randomFact;
	
	const params = {
      topic: '$aws/things/Garage/shadow/update',
      payload: '{ "state": { "desired": {"door":' + door + '}}}'
    }
	console.log('Garage ready to publish');
	iotData.publish(params, (err, res) => {
      if (err)
        console.log(err);
      else
        console.log(res);
    });

    return handlerInput.responseBuilder
      .speak(speechOutput)
      .withSimpleCard(SKILL_NAME, randomFact)
      .getResponse();
  },
};

const HelpHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak(HELP_MESSAGE)
      .reprompt(HELP_REPROMPT)
      .getResponse();
  },
};

const ExitHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'IntentRequest'
      && (request.intent.name === 'AMAZON.CancelIntent'
        || request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak(STOP_MESSAGE)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak("That's an error, you done did it fam.")
      .reprompt('I mean; sorry, an error occurred.')
      .getResponse();
  },
};

const SKILL_NAME = 'Garage';
const ALEXA_GARAGE_RESPONSE_MESSAGE = 'Signal sent. ';
const HELP_MESSAGE = 'You can say open my garage door, close my garage door, or, you can say exit... What can I help you with?';
const HELP_REPROMPT = 'What can I help you with?';
const STOP_MESSAGE = 'Goodbye!';

const dataOpen = [
  'Toast sounds good to me.',
  'I like my toast like charcoal.',
  'It should be heating up.',
  'This should be interesting.',
  'Sounds yummy.',
];

const dataClose = [
  'Toast sounds good to me.',
  'I like my toast like charcoal.',
  'It should be heating up.',
  'This should be interesting.',
  'Sounds yummy.',
];

const skillBuilder = Alexa.SkillBuilders.standard();

exports.handler = skillBuilder
  .addRequestHandlers(
    GetNewFactHandler,
    HelpHandler,
    ExitHandler,
    SessionEndedRequestHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
