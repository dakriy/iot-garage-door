#include <Wire.h>
#include <VL53L0X.h>
#include <WiFi.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include <AWS_IOT.h>
#include <WiFi.h>
#include <MQTT.h>


const int ledPin = 15;
const int relayPin = 23;
const int motionPin = 5;

VL53L0X laser;
Adafruit_BMP280 bmp;
AWS_IOT hornbill;

bool doorOpen;

char WIFI_SSID[]="hackathon2019";
char WIFI_PASSWORD[]="fearlesscoder";
char HOST_ADDRESS[]="a3drv63wrj9641-ats.iot.us-west-2.amazonaws.com";
char CLIENT_ID[]= "client id";
char TOPIC_NAME[]= "$aws/things/Garage/shadow/update";

int status = WL_IDLE_STATUS;
int tick=0,msgCount=0,msgReceived = 0;
char payload[512];
char rcvdPayload[512];
WiFiClient net;
MQTTClient mqttClient;

void mySubCallBackHandler (char *topicName, int payloadLen, char *payLoad)
{
    strncpy(rcvdPayload,payLoad,payloadLen);
    rcvdPayload[payloadLen] = 0;
    msgReceived = 1;
}

void messageReceived(String &topic, String &payload) {
  doorOpen = payload.toInt();
  if (doorOpen) {
    digitalWrite(relayPin, LOW);
  } else {
    digitalWrite(relayPin, HIGH);
  }
  Serial.println("incoming: " + topic + " - " + payload);
}

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!mqttClient.connect("arduino", "try", "try")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  mqttClient.subscribe("garage");
  // mqttClient.unsubscribe("/hello");
}

void setup() {
  Serial.begin(115200);
  Serial.println("Starting");
  pinMode(ledPin, OUTPUT);
  pinMode(motionPin, INPUT);
  digitalWrite(ledPin, LOW);
  pinMode(relayPin, OUTPUT);

  Wire.begin();
  doorOpen = false;

  while (status != WL_CONNECTED)
  {
      Serial.print("Attempting to connect to SSID: ");
      Serial.println(WIFI_SSID);
      // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
      status = WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

      // wait 5 seconds for connection:
      delay(5000);
  }
  
  Serial.println("Connected to wifi");

  mqttClient.begin("broker.shiftr.io", net);
  mqttClient.onMessage(messageReceived);

  connect();

    if(hornbill.connect(HOST_ADDRESS,CLIENT_ID)== 0)
    {
        Serial.println("Connected to AWS");
        delay(1000);

        if(0==hornbill.subscribe(TOPIC_NAME,mySubCallBackHandler))
        {
            Serial.println("Subscribe Successfull");
        }
        else
        {
            Serial.println("Subscribe Failed, Check the Thing Name and Certificates");
            while(1);
        }
    }
    else
    {
        Serial.println("AWS connection failed, Check the HOST Address");
        while(1);
    }
    
  if (!bmp.begin(0x76)) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

    bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */

  laser.init();
  laser.setTimeout(500);

  // Set for high accuracy
  laser.setMeasurementTimingBudget(200000);
}

void loop() {
  uint16_t distance = laser.readRangeSingleMillimeters();
  
  Serial.print(distance);
  
  if (laser.timeoutOccurred()) {
    Serial.print(" TIMEOUT"); 
  } else {
    if (distance < 200 && distance != 0 && digitalRead(motionPin)) {
      doorOpen = !doorOpen;
    }
  }
  
  Serial.println();

  if (doorOpen) {
    digitalWrite(ledPin, HIGH);
    digitalWrite(relayPin, HIGH);
  } else {
    digitalWrite(ledPin, LOW);
    digitalWrite(relayPin, LOW);
  }

  if(msgReceived == 1)
  {
      msgReceived = 0;
      Serial.print("Received Message:");
      Serial.println(rcvdPayload);
  }
  float temperature = bmp.readTemperature();
  float pressure = bmp.readPressure();
  float altitude = bmp.readAltitude(1013.25);
  Serial.print(F("Temperature = "));
  Serial.print(temperature);
  Serial.println(" *C");

  Serial.print(F("Pressure = "));
  Serial.print(pressure);
  Serial.println(" Pa");

  Serial.print(F("Approx altitude = "));
  Serial.print(altitude); /* Adjusted to local forecast! */
  Serial.println(" m");

  
  delay(500);

  // Update to AWS
  if(tick >= 10)   // publish to topic every 5seconds
  {
      tick=0;
      sprintf(payload,"{ \"state\": { \"desired\": {\"activate\":1}}}");
      Serial.println("Publishing");
      if(hornbill.publish(TOPIC_NAME,payload) == 0)
      {        
          Serial.print("Publish Message:");
          Serial.println(payload);
      }
      else
      {
          Serial.println("Publish failed");
      }

      // Talk to MQTT
      mqttClient.publish("garage", "Temp: " + String(temperature) + " Pressure: " + String(pressure) + " Altitude: " + String(altitude));
  }  
  
  tick++;
}
